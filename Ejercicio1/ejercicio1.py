# List of number from 1 to 100
numberList = [i for i in range(1,101)]

# List of the divisors from a number
def divisors(n):
    return [i for i in range(1, n) if n % i == 0]

# Checks the type of the number and prints the result
def check_number(num):
    divisors_sum = sum(divisors(num))
    if divisors_sum > num:
        print("%d is a Abundant Number" %num)
    elif divisors_sum < num:
        print("%d is a Deficient Number" %num)
    elif divisors_sum == num:
        print("%d is a Perfect Number" %num)

# Loops the numbers list and checks the number type
for Number in numberList:
    check_number(Number)
    