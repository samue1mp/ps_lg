
// Waits for document loads
$(document).ready(function(){
	loadDataSource();
});

/**
 * Initial function
 */
function loadDataSource () {

	// Requests to load the data
	var req1 = $.get("http://s3.amazonaws.com/logtrust-static/test/test/data1.json");
	var req2 = $.get("http://s3.amazonaws.com/logtrust-static/test/test/data2.json");
	var req3 = $.get("http://s3.amazonaws.com/logtrust-static/test/test/data3.json");
	
	// Join all request
	$.when( req1, req2, req3 ).done(function ( data1, data2, data3 ) {
		var serie1 = data1[0];
		var serie2 = data2[0];
		var serie3 = data3[0];
		
		var dataMap = new Map();

	// Loops over data serie 1
		serie1.forEach(item => {
			addPoint(dataMap, {cat: item.cat, key: item.d, value: item.value});
		});        
	// Loops over data series 2
		serie2.forEach(item => {
			addPoint(dataMap, {cat: item.categ, key: new Date(item.myDate).getTime(), value: item.val});
		});
	// Loops over data series 3
		serie3.forEach(item => {

			var cat = item.raw.match(/#(CAT\s\d)#/)[1];				// Match for category string "CAT X"
			var key = item.raw.match(/(\d{4}-\d{2}-\d{2})/)[1];		// Match for date string "YYYY-MM-DD"
			
			addPoint(dataMap, {cat: cat, key: new Date(key).getTime(), value: item.val});
		}); 

	// Translate data for higcharts
	
		var pieDataMap = new Map();

		// Translate the data structure into a highcharts data for LINE chart
		var hcData = Array.from(dataMap.entries()).map(function(obj) {
			var categoryKey = obj[0];
			
			// Sum the data for pie chart into the "map" of line chart data
			pieDataMap.set(categoryKey, Array.from(obj[1].values()).reduce((a, b) => a + b, 0));
			
			// return object for line higcharts
			return {    "name": categoryKey,
						"data": Array.from(obj[1].entries()).sort()	// Sorted array of points
			};
		});

		// Translate the data structure into a highcharts data for PIE chart
		var hcPieData = [{
			name: 'Brands',
        	colorByPoint: true,
        	data: Array.from(pieDataMap.entries()).map(function(obj) {
				// return object for pie higcharts
				return {	"name": obj[0],					// categoryKey
							"y": obj[1],
							sliced: obj[0] == "CAT 2",		// Sliced category
            				selected: obj[0] == "CAT 2"		// Selected category
				};
			})
		}];

		// Creates the line and pie charts
		createLineChart(hcData);
		createPieChart(hcPieData);

	});
}

/**
 * Helper to add a point with a category and a date
 * @param {*} dataMap 
 * @param {*} item 
 */
function addPoint(dataMap, item) {
	// Some validations
	if (dataMap && item && item.cat && item.key && item.value) {

		item.cat = item.cat.toUpperCase();	// Normalized category name to upper case

		if (!dataMap.has(item.cat)) {				// Current category not exist
			dataMap.set(item.cat, new Map());
		}
		if (dataMap.get(item.cat).has(item.key)) {	// Current key(date) exist
			var currValue = dataMap.get(item.cat).get(item.key);
			dataMap.get(item.cat).set(item.key, currValue + item.value);	// Adding value to the current one for this key(date)
		} else {
			dataMap.get(item.cat).set(item.key, item.value);				// New date point
		}
	}
}

/**
 * Creates a line highcharts based on hcData parameter
 * @param {*} hcData 
 */
function createLineChart(hcData) {
	Highcharts.chart({
		chart: {
			renderTo: 'lineContainer',
			type: 'line'
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: false
				}
			}
		},
		xAxis: {
			type: 'datetime',
			dateTimeLabelFormats: { 
				month: '%e. %b',
				year: '%b'
			},
			title: {
				text: 'Date'
			}
		},
		legend: {
			align: 'right',
			verticalAlign: 'middle',
			layout: 'vertical',
		},
		title: {
			text: 'LINE - Ejercicio 2'
		},
		series: hcData
	});
}

/**
 * Creates a pie highcharts based on hcData parameter
 * @param {*} hcData 
 */
function createPieChart (hcData) {
	console.log(hcData);
	Highcharts.chart('pieContainer', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title: {
			text: 'PIE - Ejercicio 2'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {
						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
					}
				}
			}
		},
		series: hcData
	});
}