# Prueba de Código DEVO

## Ejercicio 1
Dentro de la carpeta Ejercicio1 he incluido el fichero .py ejecutable y que imprime en la consola el resultado de los números del 1 al 100.

Para ejecutar el ejercicio, mediante la consola, entrar en la carpeta Ejercicio1 y ejecutar `python ejercicio1.py`

## Ejercicio 2
Para la realización del segundo ejercicio utilizaré como librería Javascript **jQuery**.
Se incluye también highcharts para la representación de las gráficas.

El script ejercicio2.js contiene el código javascript del ejercicio y está dentro de la carpeta Ejercicio2/js.

Para ejecutar el ejercicio, abrir en el navegador el fichero index.html que se encuentra dentro de la carpeta Ejercicio2.
